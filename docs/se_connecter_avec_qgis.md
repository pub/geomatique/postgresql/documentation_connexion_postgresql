## Accéder à PostgreSQL avec QGIS
Avant de pouvoir afficher dans QGIS les données stockées dans une base PostgreSQL, il va falloir nous connecter à la base de données. 

### Etablir une connexion
Dans QGIS, vous pouvez établir une nouvelle connexion vers PostgreSQL:

* en cliquant sur l'outil "Gestionnaire des sources de données" disponible dans la barre d'outils ![bouton_données](./img/bt_source_data.png)
* par le menu "Couche --> Gestionnaire des sources de données" (accès rapide: Ctrl + l)
  
Dans l'interface de gestion des différentes sources de données, on opte pour PostgreSQL ![source_postgresql](./img/source_pg.png)

Cliquer sur "Nouveau" pour définir une nouvelle connexion.

![QGIS_nouvelle_connexion](./img/qgis_new_connexion.png)

Renseigner les différents paramétres nécessaires à la connexion.

![QGIS_parametres](./img/qgis_param_connect.png)

1. *Nom* : renseigner le nom que vous souhaitez. Il vous servira à retrouver la connexion à votre base de données dans la liste des connexions disponibles dans QGIS. 
2. *Hôte* : indiquer l'adresse IP du serveur sur lequel se trouve la base de données. Si votre base de données est sur votre poste de travail, renseigner "127.0.0.1" ou "localhost"
3. *Port*: il s'agit du port d'écoute de PostgreSQL sur le serveur. Dans l'offre Eole et par défaut, il s'agit du port 5432
4. *Base de données*: indiquer le nom de la base de données à laquelle vous souhaitez vous connecter.
5. *Nom d'utilisateur*: dans la section *Authentification*, onglet *De base*, saisir le rôle de connexion PostgreSQL. Ce rôle doit, à minima, disposer de la possibilité de se connecter à la base de données. Pour éviter la saisie à chaque connexion du rôle de connexion, cochez la case *Stocker*
6. *Mot de passe*: mot de passe pour le rôle de connexion
7. Cocher l'option *Lister les tables sans géométries* si vous souhaitez visualiser les tables contenant des données tabulaires
8. Permet de tester la connexion avant de l'enregistrer. 

Une fois votre connexion établie vers une base de données, vous avez plusieurs possibilités pour charger vos données dans le canevas de cartes de QGIS.

### Utiliser la connexion avec le gestionnaire de source de données

![chgt_gestionnaire](./img/chargt_gestionnaire.png)

1. Après avoir choisi la connexion vers la base de données dans la liste des connexions disponibles, cliquer sur *Connecter* 
2. La liste des ressources disponibles dans la base de données pour votre rôle de connexion apparaît. Sélectionner l'une de ces ressources.
3. Ajouter la ressource dans le canevas des cartes de QGIS

### Utiliser la connexion avec l'explorateur de QGIS
Vous pouvez activer le panneau de l'explorateur de ressources de QGIS en allant dans le menu *Vue -> Panneaux -> Explorateur*. Cet explorateur propose un onglet *PostgreSQL* dans lequel vous pourrez consulter la liste des ressources disponibles dans une connexion.

![Explorateur_QGIS](./img/qgis_explorateur.png)

Sélectionnez une ressource et glissez là dans le canevas de cartes de QGIS.

### Utiliser la connexion avec DBManager
L'outil DBManager est accessible par le menu *Base de données* de QGIS.
Son fonctionnement est similaire à l'explorateur; en sélectionnant une connexion, vous voyez les ressources disponibles de votre base de données pour votre rôle de connexion. 
Un simple click droit sur la ressource permet de l'ajouter au canevas de cartes de QGIS.

![QGIS_DBManager](./img/qgis_dbmanager.png)


