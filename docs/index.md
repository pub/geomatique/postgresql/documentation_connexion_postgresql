# Présentation
---------------

Cette documentation détaille comment se connecter à PostgreSQL avec les principaux clients

PostgreSQL est le principal système de gestion de bases de données utilisé par les services pour gérer leur patrimoine de données. Plusieurs outils tiers permettent de s'y connecter pour gérer les différents objets d'une base, interroger, manipuler et représenter les données.

Ce tutoriel présente les modalitès de connexion à une base de données à partir :

* de QGIS, logiciel SIG libre pour interroger et visualiser des données 
* de pgAdmin, principal outil open-source de gestion de bases de données pour PostgreSQL. Les principales [fonctionnalités de pgAdmin](https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/postgresql/documentation_pgadmin) sont présentées en cliquant sur le lien.
* de Libre-Office, suite bureautique libre.


