## pgAdmin: un outil, deux versions
L'application pgAdmin existe en deux versions:

* en mode "poste de travail": l'application s'installe sur le poste de travail et mémorise les connexions définies vers vos bases PostgreSQL.
* en mode "serveur": l'application est accessible au travers d'un navigateur. Elle nécessite un 1er compte de connexion qui ouvre l'accès à l'interface de pgAdmin et vous présente vos connexions définies vers vos bases PostgreSQL. C'est ce mode qui est proposé avec l'installation d'un serveur Eole PostgreSQL.

Dans les deux modes, une fois connectée au serveur de bases de données, l'application vous présentera les bases de données existantes. Vous ne pourrez vous y connecter que si votre rôle de connexion est autorisé.

### Le mode "poste de travail"
L'application est installée sur le poste de travail et vous propose les connexions mémorisées vers vos serveurs gestionnaires de bases de données (SGBD). Chaque serveur peut proposer de une à plusieurs bases de données.

![pg_admin_poste_de_travail](./img/pgadmin_poste.png)


### Le mode "serveur"
L'application pgAdmin Server vous est proposée avec votre serveur Eole PostgreSQL ou pour certaines formations. Elle est accessible depuis un navigateur à l'adresse
**http://ip-de-mon-serveur/pgadmin4**.
![pg_admin_serveur](./img/pgadmin_serveur.png)

Dans ce mode "serveur", l'application pgAdmin vous demandera de vous authentifier avant de vous présenter la liste de vos connexions mémorisées.
Ce 1er compte de connexion vous sera fourni par votre service support (ou tuteurs).

![acceuil](./img/acceuil.png "Ecran d'acceuil")

Dans le cas d'une première utilisation de pgAdmin avec l'offre Eole PostgreSQL, ce compte doit être utilisé comme compte de connexion provisoire, le temps pour vous de déclarer votre propre adresse de messagerie.
Il est conseillé de supprimer ce compte provisoire à l'issue.

> Dans le cadre de la FOAD PostgreSQL ou de Initiation SQL, l'adresse de l'application est http://foad-postgresql.e2.rie.gouv.fr/pgadmin4 .Les tuteurs vous fourniront les informatons nécessaires à la connexion. Pour la formation, ce compte n'est qu'un compte "user"; il ne vous permettra pas de créer de nouveaux comptes de connexion.

### Connexion vers une base PostgreSQL
Que vous soyez en mode "poste de travail" ou en mode "serveur", vous serez amené à créer de nouvelles connexions vers vos bases de données.
Une connexion se compose de plusieurs informations telles que:
* l'adresse IP du serveur. 
  Si vous utilisez un pgAdmin en mode "poste de travail", il vous faudra donc connaître l'adresse du serveur hébergeant PostgreSQL. Si vous utilisez pgAdmin au travers de votre navigateur (mode "serveur"), la configuration la plus fréquente est que l'application pgAdmin et PostgreSQL soient intallés sur le même serveur. Dans ce cas de figure, pgAdmin interrogeant PostgreSQL localement, l'adresse IP du serveur sera 127.0.0.1
* le port d'écoute de PostgreSQL (par défaut, on utilise le port 5432)
* le rôle de connexion autorisé à se connecter à une ou des bases de données. En effet, si pgAdmin vous présente la liste des bases disponibles sur le serveur, vous ne pourrez y accéder que si le rôle déclaré dans la connexion dispose des privilèges.
* le mot de passe pour le rôle de connexion
* le nom de la base de données sur laquelle vous souhaitez vous connecter.


## Créer une connexion
Après ouverture de l'application pgAdmin, sur le panneau de gauche, vous disposez d'un menu de création de connexion vers une base de données.
![nouvelle_connexion](./img/new-connexion.png)
Dans l'onglet Général, vous renseignerez le champ Nom pour identifier la connexion pour le serveur. Nous vous recommandons de mettre à minima le nom de la base, le rôle de connexion voire l'IP du serveur afin de vous rappeler ultérieurement sur quelle base et avec quel rôle vous souhaitez vous connecter. Exemple: baseFF_adl
![general_nouvelle_connexion](./img/general-new-connexion.png)

Si l'option *Connecter maintenant* est cochée (c'est la valeur par défaut), pgAdmin tentera de se connecter à la base de données de votre serveur lorsque vous fermerez cette boîte de dialogue.

En mode serveur uniquement, pgAdmin propose aux utilisateurs de l'application ayant un profil *Administrator* une option *Partagé*. Si cette option est cochée (ce n'est pas la valeur par défaut), la connexion sera partagée entre tous les utilisateurs de pgAdmin. Les utilisateurs bénéficiant de cette déclaration partagée de connexion ne pourront ni supprimer cette connexion, ni la renommer ou changer les informations relatives au serveur, port ou base de connexion.


C'est réellement dans l'onglet Connexion que l'on définit les paramètres nécessaires à une connexion vers une base pour un rôle de connexion PostgreSQL.
![connexion_onglet](./img/connexion-new-connexion.png)

Le champ *Nom d'hôte* permet de spécifier l'adresse IP du serveur qui héberge les bases de données que vous souhaitez joindre. Si votre connexion a pour objectif de vous connecter à une base de données hébergée sur le même serveur que celui sur lequel pgAdmin est installé, vous pouvez indiquer comme adresse localhost ou 127.0.0.1. Si vous souhaitez joindre une base hébergée sur un serveur distant, il faut alors spécifier l'adresse IP du serveur à joindre.

Dans l'offre Eole, le *port* d'écoute du serveur pour PostgreSQL est 5432. C'est la valeur par défaut.

Le champ *Base de données de maintenance* indique le nom de la base de données initiale à laquelle le rôle de connexion se connectera. Par défaut, l'offre Eole PostgreSQL fournit une première base de données nommée adl.

Le champ *Nom utilisateur* attend un rôle de connexion PostgreSQL autorisé à se connecter à la base définie précédemment.

Le champ *Mot de passe* est le mot de passe nécessaire au rôle de connexion précédemment défini pour se connecter. Ce mot de passe est géré dans PostgreSQL. En activant *Enregistrer le mot de passe*, vous demandez à pgAdmin de sauvegarder ce mot de passe pour une utilisation ultérieure, vous évitant une saisie à chaque demande de connexion. Par souci de sécurité, cette option est désactivée par défaut.

Une fois ces informations saisies, vous pouvez stocker votre connexion avec le bouton Enregistrer. A chaque nouvelle connexion à pgAdmin, vous retrouverez vos connexions mémorisées vers PostgreSQL disponibles.