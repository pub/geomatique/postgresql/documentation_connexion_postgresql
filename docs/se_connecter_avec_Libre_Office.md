## Accéder à PostgreSQL avec Libre Office
La suite bureautique Libre Office propose différents outils qui peuvent se connecter à une base de données PostgreSQL, et notamment:
* un tableur Calc qui permet de faire du traitements et analyses de données
* un module Base proposant de réaliser des formulaires sur une base de données

### Créer une connexion
La création d'une connexion vers une base de données PostgreSQL s'effectue par Libre Office Base.
Si vous ne disposez pas de connexion déjà établie, Libre Office Base vous propose, au lancement, son *Assistant Base de données* pour établir une connexion. 
Il est possible, à tout moment, de créer une nouvelle connexion par le menu *Fichier -> Nouveau -> Bases de données*.

Opter pour *"Connecter une base de données existante"* et choisir PostgreSQL

![LibreOffice_connexion](./img/lo_connexion.png)

Fournir les informations pour la connexion

![LibreOffice_params](./img/lo_param.png)

* *dbname* : nom de la base de données
* *host* : adresse IP du serveur. Si PostgreSQL est installé sur le poste de travail, on peut utiliser "127.0.0.1" ou "localhost"
* *port* : port d'écoute de PostgreSQL sur le serveur. Pour l'offre Eole et d'une manière génèrale, on utilise le port 5432

Donner le rôle de connexion

![LibreOffice_rôle](./img/lo_user.png)
Penser à cocher *Mot de passe requis". Le bouton *Tester la connexion" permet de contrôler la validité des informations de connexions avant mémorisation.

L'assistant Libre Office vous demande un répertoire d'enregistrement des paramètres de connexion vers votre base de données qui sont stockés dans un fichier au format OpenDocument Database (.odb)

### Etablir une connexion avec Calc
Après ouverture d'une nouvelle feuille Libre Office Calc, la liste des connexions disponibles est visible via le menu *Affichage -> Sources de données" (raccourci Ctrl+Maj+F4).
Vous y retrouver la connexion établie vers la base de données

![LibreOffice](./img/libreoffice_consult.png)
1. Liste des connexions disponibles. Sélectionner l'une d'elles pour voir les tables
2. Liste des tables disponibles pour le rôle de connexion
3. Prévisualisation des données. Obtenue en cliquant sur l'une des tables disponibles
4. Chargement des données dans la feuille de calcul par glisser-déplacer de la table vers la feuille
