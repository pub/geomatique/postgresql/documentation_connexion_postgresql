# Les sites utiles
------------------

## Références

* pgAdmin: [les principales fonctionnalités](https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/postgresql/documentation_pgadmin/)
  
* Site communautaire pgAdmin: [site pgadmin](https://www.pgadmin.org/)

* Site communautaire Qgis: [site Qgis](https://www.qgis.org/)

* Site communautaire Libre Office: [site Libre Office](https://fr.libreoffice.org/)